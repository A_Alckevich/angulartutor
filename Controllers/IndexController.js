var myApp = angular.module("myApp", []);

myApp.controller("IndexController", function($scope){
    $scope.people = [
        {name: 'David', city: 'New York', isOnline: true},
        {name: 'Roma', city: 'Argentina', isOnline: true},
        {name: 'Alex', city: 'Warsaw', isOnline: false}
    ];
    
    $scope.sortField = undefined;
    $scope.reverse = false;
    
    $scope.setSortField = function(sortField){
        $scope.reverse = !$scope.reverse;
        $scope.sortField = sortField;
    };
    
    $scope.shownPeople = "all";
    
    $scope.showNextPeople = function(){
        if($scope.shownPeople === "all") $scope.shownPeople = "online";
        else if($scope.shownPeople === "online") $scope.shownPeople = "offline";
        else if($scope.shownPeople === "offline") $scope.shownPeople = "all";
    };
    
    $scope.myFilter = function(human){
        if($scope.shownPeople === "all") return true;
        if($scope.shownPeople === "online") return human.isOnline;
        if($scope.shownPeople === "offline") return !human.isOnline;
    };
    
 });
